package agents;

import loveletter.*;
import java.util.Random;
import java.util.PriorityQueue;
import java.util.Comparator;

/**
 * An interface for representing an agent in the game Love Letter All agent's
 * must have a 0 parameter constructor
 * 
 * This will aim to become a knowledged based agent.
 */
public class KnowledgeAgent implements Agent {

	private State current;
	private int myIndex;
	private Card currentCard;
	private Card newCard;
	private int target;
	private Card play;

	public KnowledgeAgent() {
		for (Card c : Card.values())
			System.out.println(c);
	}

	/**
	 * Reports the agents name
	 */
	public String toString() {
		return "Knowledge Agent";
	}

	/**
	 * Method called at the start of a round
	 * 
	 * @param start the starting state of the round
	 **/
	public void newRound(State start) {
		current = start;
		myIndex = current.getPlayerIndex();
	}

	/**
	 * Method called when any agent performs an action.
	 * 
	 * @param act     the action an agent performs
	 * @param results the state of play the agent is able to observe.
	 **/
	public void see(Action act, State results) {
		current = results;
	}

	/**
	 * Perform an action after drawing a card from the deck
	 * 
	 * @param c the card drawn from the deck
	 * @return the action the agent chooses to perform
	 * @throws IllegalActionException when the Action produced is not legal.
	 */
	public Action playCard(Card c) {
		Action act = null;
		currentCard = current.getCard(myIndex);
		newCard = c;
		pickAction();
		try {
			switch (play) {
			case GUARD:
				act = Action.playGuard(myIndex, target, Card.values()[1]);
				break;
			case PRIEST:
				act = Action.playPriest(myIndex, target);
				break;
			case BARON:
				act = Action.playBaron(myIndex, target);
				break;
			case HANDMAID:
				act = Action.playHandmaid(myIndex);
				break;
			case PRINCE:
				act = Action.playPrince(myIndex, target);
				break;
			case KING:
				act = Action.playKing(myIndex, target);
				break;
			case COUNTESS:
				act = Action.playCountess(myIndex);
				break;
            default:
                act = Action.playPrincess(myIndex);
            }
            return act;

		} catch (IllegalActionException e) {
			System.out.println("Illegal Action occured: " + e.toString());
            RandomAgent a = new RandomAgent();
            a.newRound(current);
            return a.playCard(c);
        }
	}


	public void pickAction(){
		//play the least value
		boolean whichCard;
		if (newCard.value() > currentCard.value()) {
			play = currentCard; 
			whichCard = true;
		} else {
			whichCard = false; 
			play = newCard;
		}

		if (play.value() > 4) {
			if (newCard.value() == 7 ) {
				whichCard = false;
				play = newCard;
			}
			else if (currentCard.value() == 7) {
				whichCard = true;
				play = currentCard;
			}
		}	

		/*
		if one opponent left, and opponent plays handmaiden, and we cannot play against them or ourselves
		*/
		if(decide(play)){
			this.target = pickTarget();
			if (this.target == myIndex) {
                if (play == play.PRINCE) {
                    return;
                }
				play = whichCard ? newCard : currentCard;
				this.target = decide(play) ? pickTarget() : myIndex;
			}
		}
	}
	
	public boolean decide(Card myCard){
		switch (myCard) {
			case GUARD:
				return true;
			case PRIEST:
				return true;
			case BARON:
				return true;
			case HANDMAID:
				return false;
			case PRINCE:
				return true;
			case KING:
				return true;
			case COUNTESS:
				return false;
			case PRINCESS:
				return false;
		}
		return false;
	}

	public int pickTarget() {
		// pick player to play card against
		int testTarg = 0;
		int noTargets = current.numPlayers();
		try {
			int counter = 1;
			do {
				if (counter > 8) {
					return myIndex;
				}
				testTarg = counter % 4;
				counter++;
			} while (current.eliminated(testTarg) || current.handmaid(testTarg) || testTarg == myIndex); // while target not eliminated,doesn't have handmaiden, and isn't yourself
			return testTarg;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Target failed");
			return myIndex;
		}
	}
}
