*We left our other agents used for testing in this directory.*

**Lachlan Russell   | 22414249**
**Dean Vains        | 22273794**

## Our submissions:

### Agent 1: LogicAgent.java
This is our knowledge/logic based agent. 

### Agent 2: GeneticAgent.java
This is also a knowledge agent but has been trained via LoveLetter/src/loveletter/GeneticTrainer.java


## Run the project
We have included a makefile as a replacement for your build script.
`make` Will build and run the project. The `GeneticTrainer` class will call the `LoveLetter` game.
It will print the Q1 Average results from the population of tested agents and the generation number.

You can modify which agents are run through the `LoveLetter` class (to change the proportion of wins printed to screen, the old main function now returns the proportion, so you can modify the index for which agent you want to know the win rate.

There are also other targets in make to build documentation.

The zero paramater constructor for `GeneticAgent` constructs the genetic agent with some offline computed chromosomes.


