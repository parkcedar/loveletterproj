
package agents;

import loveletter.*;
import java.util.Random;
import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.Arrays;
import java.lang.Math;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.LinkedList;

/**
* An interface for representing an agent in the game Love Letter All agent's
* must have a 0 parameter constructor
* 
*/
public class LogicAgent implements Agent {
    
    private State current;
    private int myIndex;
    private Card currentCard;
    private Card newCard;
    private int target;
    private Card play;
    private Card guess = Card.values()[2];

    private int numPlayers = 4;
    Card[] unseen; //array storing the unseen cards
    private double[][] playerChances = new double[4][8]; //array to store probabilities
    
    public LogicAgent() {
//        for (Card c : Card.values())
//        System.out.println(c);
    }
    
    /**
    * Reports the agents name
    */
    public String toString() {
        return "Logic Agent";
    }
    
    /**
    * Method called at the start of a round
    * 
    * @param start the starting state of the round
    **/
    public void newRound(State start) {
        current = start;
        myIndex = current.getPlayerIndex();
    }
    
    /**
    * Method called when any agent performs an action.
    * This method counts the cards and calculates the probability of each player
    * having each card and stores them in the playerChance variable

    * @param act     the action an agent performs
    * @param results the state of play the agent is able to observe.
    **/
    public void see(Action act, State results) {
        current = results;
        numPlayers = current.numPlayers();
        Card[] unseen = current.unseenCards();
        int remaining = current.unseenCards().length;
        int[] tempCards = new int[8];
        double[] chance = new double[8];
        

        for(int i = 0; i < remaining; i++){
            if(unseen[i].value() == 1){
                tempCards[0]++;
            }else if(unseen[i].value() == 2){
                tempCards[1]++;
            }else if(unseen[i].value() == 3){
                tempCards[2]++;
            }else if(unseen[i].value() == 4){
                tempCards[3]++;
            }else if(unseen[i].value() == 5){
                tempCards[4]++;
            }else if(unseen[i].value() == 6){
                tempCards[5]++;
            }else if(unseen[i].value() == 7){
                tempCards[6]++;
            }
            else if(unseen[i].value() == 8){
                tempCards[7]++;
            }
        }
        
        for(int j = 0; j < 7; j++){
            chance[j] = (double)tempCards[j] / (double)remaining;
            playerChances[0][j] = chance[j];
            playerChances[1][j] = chance[j];
            playerChances[2][j] = chance[j];
            playerChances[3][j] = chance[j];
            
        }
        
        Card actCard = act.card();
        int actPlayer = act.player();
        
        
        /**
         * Basic heurist calculations, exact probabilities in here could be refined
         * 
         */
        for(int k = 0; k < 4; k++){
            if(actPlayer == k){
                if(actCard.value() == 1){
                    playerChances[0][0]-= 0.05;
                    playerChances[1][0]-= 0.05;
                    playerChances[2][0]-= 0.05;
                    playerChances[3][0]-= 0.05;
                    
                }else if(actCard.value() ==2){
                    playerChances[0][1]-= 0.1;
                    playerChances[1][1]-= 0.1;
                    playerChances[2][1]-= 0.1;
                    playerChances[3][1]-= 0.1;
                    
                }else if(actCard.value() ==3){
                    playerChances[0][2]-= 0.1;
                    playerChances[1][2]-= 0.1;
                    playerChances[2][2]-= 0.1;
                    playerChances[3][2]-= 0.1;
                    
                }else if(actCard.value() ==4){
                    playerChances[0][1]-= 0.1;
                    playerChances[1][1]-= 0.1;
                    playerChances[2][1]-= 0.1;
                    playerChances[3][1]-= 0.1;
                    
                }else if(actCard.value() == 5){
                    playerChances[k][0]-= 0.1;
                    playerChances[k][1]-= 0.1;
                    playerChances[k][2]-= 0.1;
                    playerChances[k][3]-= 0.1;
                    playerChances[k][5]+= 0.1;
                    playerChances[k][6]+= 0.15;
                    playerChances[k][7]+= 0.15;
                    
                    playerChances[0][4]-= 0.1;
                    playerChances[1][4]-= 0.1;
                    playerChances[2][4]-= 0.1;
                    playerChances[3][4]-= 0.1;
                    
                }else if(actCard.value() == 6){
                    playerChances[k][0]+= 0.1;
                    playerChances[k][1]+= 0.1;
                    playerChances[k][2]+= 0.1;
                    playerChances[k][3]+= 0.1;
                    playerChances[k][4]-= 0.4;
                    playerChances[k][6]+= -1.0;
                    playerChances[k][7] += 0.1;
                    
                    playerChances[0][5]-= 1.0;
                    playerChances[1][5]-= 1.0;
                    playerChances[2][5]-= 1.0;
                    playerChances[3][5]-= 1.0;
                    
                }else if(actCard.value() ==7){
                    playerChances[k][0]-= 0.1;
                    playerChances[k][1]-= 0.1;
                    playerChances[k][2]-= 0.1;
                    playerChances[k][3]-= 0.1;
                    playerChances[k][4]-= 0.1;
                    playerChances[k][5]-= 0.2;
                    playerChances[k][6]+= 0.15;
                    playerChances[k][7]+= 0.15;
                    
                    playerChances[0][5]-= 1.0;
                    playerChances[1][5]-= 1.0;
                    playerChances[2][5]-= 1.0;
                    playerChances[3][5]-= 1.0;
                }
            }
        }
        
        
        //rounds the probabilities so you can have a negative, or a probability above 100%
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 8; j++){
                if(playerChances[i][j] < 0) playerChances[i][j] = 0; 
                if(playerChances[i][j] > 1) playerChances[i][j] = 1;
                playerChances[i][j] = Math.round(playerChances[i][j] * 10) / 10.0;
            }
            
        }
        
        
    }
    
    /**
    * Perform an action after drawing a card from the deck
    * 
    * @param c the card drawn from the deck
    * @return the action the agent chooses to perform
    * @throws IllegalActionException when the Action produced is not legal.
    */
    public Action playCard(Card c) {
        Action act = null;
        currentCard = current.getCard(myIndex);
        newCard = c;
        pickAction();
        
        try {
            switch (play) {
                case GUARD:
                act = Action.playGuard(myIndex, target, guess);
                break;
                case PRIEST:
                act = Action.playPriest(myIndex, target);
                break;
                case BARON:
                act = Action.playBaron(myIndex, target);
                break;
                case HANDMAID:
                act = Action.playHandmaid(myIndex);
                break;
                case PRINCE:
                act = Action.playPrince(myIndex, target);
                break;
                case KING:
                act = Action.playKing(myIndex, target);
                break;
                case COUNTESS:
                act = Action.playCountess(myIndex);
                break;
                default:
                act = Action.playPrincess(myIndex);
            }
            return act;
            
        } catch (IllegalActionException e) {
//            System.out.println("Illegal Action occured: " + e.toString());
            return playCard(Card.PRINCE);
        }
    }
    
    
    public void pickAction(){

        Card[] unseen = current.unseenCards();
        int remaining = current.unseenCards().length;


        //play the least value
        boolean whichCard;
        if (newCard.value() > currentCard.value()) {
            play = currentCard; 
            whichCard = true;
        } else {
            whichCard = false; 
            play = newCard;
        }



        //always play the handmaiden
        if (newCard.value() == 4) play = newCard;
        else if (currentCard.value() == 4) play = currentCard;

        //never disgard the princess
        else if(newCard.value() == 8) play = currentCard;
        else if (currentCard.value() == 8) play = newCard;
        
        //play the baron early
        else if(newCard.value() == 3 && remaining > 10) play = newCard;
        else if(currentCard.value() == 3 && remaining > 10) play = currentCard;

        //Play countess if you have high cards
        if (play.value() > 4) {
            if (newCard.value() == 7 ) {
                whichCard = false;
                play = newCard;
            }
            else if (currentCard.value() == 7) {
                whichCard = true;
                play = currentCard;
            }
        }	

        //play baron if you have a high card
        if(currentCard.value() == 3 && newCard.value() > 6) play = currentCard;
        if(newCard.value() == 3 && currentCard.value() > 6) play = newCard;


        
        /*
        if one opponent left, and opponent plays handmaiden, and we cannot play against them or ourselves
        */
        if(decide(play)){
            this.target = pickTarget();
            if (this.target == myIndex) {
                if (play == play.PRINCE) {
                    return;
                }
                play = whichCard ? newCard : currentCard;
                this.target = decide(play) ? pickTarget() : myIndex;
            }
        }
    }
    
    public boolean decide(Card myCard){
        switch (myCard) {
            case GUARD:
            return true;
            case PRIEST:
            return true;
            case BARON:
            return true;
            case HANDMAID:
            return false;
            case PRINCE:
            return true;
            case KING:
            return true;
            case COUNTESS:
            return false;
            case PRINCESS:
            return false;
        }
        return false;
    }
    
    /**
     * 
     * This method picks the best target to play the chosen cards against, based
     * off of the probabilities calculated each round.
     * 
     * @return Returns the target players index.
     */
    public int pickTarget() {
        // pick player to play card against
        int testTarg = -1;
        int noTargets = current.numPlayers();
        
        switch (play.value()) {
            case 1:
            int case1b1 = -100;
            int case1b2 = -100;
            double case1best = -100;
            
            for(int i = 0; i < 4; i++){
                
                for(int j = 0; j < 8; j++){
                    //Does not bother searching for guards
                    if(j == 0 || current.eliminated(i) || current.handmaid(i) || i == myIndex){
                        
                        continue;
                    }else{
                        //if the current probability is better, update
                        if(playerChances[i][j] > case1best){
                            case1b1= i;
                            case1b2 = j;
                            case1best = playerChances[i][j];
                            
                            
                        }
                    }
                }
            }
            
            //used the convert card number guess in correct syntax
            switch(case1b2){
                case 1:
                guess = Card.values()[1];
                case 2:
                guess = Card.values()[2];
                case 3:
                guess = Card.values()[3];
                case 4:
                guess = Card.values()[4];
                case 5:
                guess = Card.values()[5];
                case 6:
                guess = Card.values()[6];
                case 7:
                guess = Card.values()[7];
            }
            
            if(case1b1 == -100){case1b1=myIndex;} //if cannot find valid target, target itself and forfeit
            return case1b1;
            
            
            case 2:
            int counter = 1;
            do {
                if (counter > 8) {
                    return myIndex;
                }
                testTarg = counter % 4;
                
                
                counter++;
            } while (current.eliminated(testTarg) || current.handmaid(testTarg) || testTarg == myIndex);
            return testTarg;
            
            
            case 3:
            int case3b1 = -100;
            int case3b2 = -100;
            double case3best = -100;
            int myCard = 0;
            if(currentCard.value() == 3){
                myCard = newCard.value();
            }else{
                myCard = currentCard.value();
            }
            
            for(int i = 0; i < 4; i++){
                
                for(int j = 0; j < 8; j++){
                    //Dont search for higher score cards as this will elimate the agent
                    if(j > myCard || current.eliminated(i) || current.handmaid(i) || i == myIndex){
                        continue;
                    }else{
                        
                        if(playerChances[i][j] > case3best){
                            case3b1= i;
                            case3b2 = j;
                            case3best = playerChances[i][j];
                            
                        }
                    }
                }
            }
            if(case3b1 == -100){case3b1 = myIndex;}
            return case3b1;
            
            
            case 5:
            int case5b1 = -100;
            int case5b2 = -100;
            double case5best = -100;
            
            for(int i = 0; i < 4; i++){
                
                for(int j = 0; j < 8; j++){
                    //look for players with high probability hands
                    if(current.eliminated(i) || current.handmaid(i) || i == myIndex){
                        continue;
                    }else{
                        
                        if(playerChances[i][j] > case5best || (j > 4 && playerChances[i][j] > 0.6)){
                            case5b1= i;
                            case5b2 = j;
                            case5best = playerChances[i][j];
                            
                        }
                        
                    }
                }
            }
            if(case5b1 == -100){case5b1 = myIndex;}
            return case5b1;
            
            
            case 6:
            int case6b1 = -100;
            int case6b2 = -100;
            double case6best = -100;
            if(currentCard.value() == 3){
                myCard = newCard.value();
            }else{
                myCard = currentCard.value();
            }
            
            for(int i = 0; i < 4; i++){
                
                for(int j = 0; j < 8; j++){
                    if(current.eliminated(i) || current.handmaid(i) || i == myIndex){
                        continue;
                    }else{
                        //Get any initial match, then only pick high probability cards that are higher than the agents.
                        if((playerChances[i][j] > case6best && case6b1 < 3) || (playerChances[i][j] > 0.6 && j > myCard )){
                            case6b1= i;
                            case6b2 = j;
                            case6best = playerChances[i][j];
                            
                        }
                    }
                }
            }
            if(case6b1 == -100){case6b1=myIndex;}
            return case6b1;
            
            
            default:
            return 0;
            
        }
    }
    
    
}
