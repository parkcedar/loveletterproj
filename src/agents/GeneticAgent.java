package agents;

import loveletter.*;
import java.util.Random;
import java.util.PriorityQueue;
import java.util.Comparator;

/**
 * An interface for representing an agent in the game Love Letter All agent's
 * must have a 0 parameter constructor
 * 
 * This will aim to become a knowledged based agent.
 */
public class GeneticAgent implements Agent {

	private State current;
	private int myIndex;

	private PriorityQueue<Card> myCards; 
    private Card one;
    private Card two;

    int target;
    int targetCard;
	private Card play;


    // Chromosomes
    private double[] chrome = {100, 80, 3, 3}; // From training data.

	public GeneticAgent() {
	}

    public GeneticAgent(double[] chrom) {
        this.chrome = chrom;
//        System.out.println(A + ":" + B + ":" + C + ":" + D);
    }


	/**
	 * Reports the agents name
	 */
	public String toString() {
		return "Genetic Agent";
	}


	/**
	 * Method called at the start of a round
	 * 
	 * @param start the starting state of the round
	 **/
	public void newRound(State start) {
		current = start;
		myIndex = current.getPlayerIndex();
	}

	/**
	 * Method called when any agent performs an action.
	 * 
	 * @param act     the action an agent performs
	 * @param results the state of play the agent is able to observe.
	 **/
	public void see(Action act, State results) {
		current = results;
	}


	/**
	 * Perform an action after drawing a card from the deck
	 * 
	 * @param c the card drawn from the deck
	 * @return the action the agent chooses to perform
	 * @throws IllegalActionException when the Action produced is not legal.
	 */
	public Action playCard(Card c) {
		Action act = null;
        
        // Initialise a new list and add our cards to it.
        myCards = new PriorityQueue<Card>(2);
		myCards.add(current.getCard(myIndex));
		myCards.add(c);

        // Copy the queue so we have both cards for easy access
        PriorityQueue<Card> copy = new PriorityQueue<>(myCards);
        one = copy.poll();
        two = copy.poll();
        

        // Begin to choose moves.
		pickAction();
		try {
			switch (play) {
			case GUARD:
				act = Action.playGuard(myIndex, target, Card.values()[targetCard - 1]);
				break;
			case PRIEST:
				act = Action.playPriest(myIndex, target);
				break;
			case BARON:
				act = Action.playBaron(myIndex, target);
				break;
			case HANDMAID:
				act = Action.playHandmaid(myIndex);
				break;
			case PRINCE:
				act = Action.playPrince(myIndex, target);
				break;
			case KING:
				act = Action.playKing(myIndex, target);
				break;
			case COUNTESS:
				act = Action.playCountess(myIndex);
				break;
            default:
                act = Action.playPrincess(myIndex);
            }
            return act;

		} catch (IllegalActionException e) {
			System.out.println("Illegal Action occured (GeneticAgent): " + e.toString());
            System.exit(1);
        }
        return act;
	}

    //
    // Heuristics:
    //

    public int diff() {
        return Math.abs(one.value() - two.value() * 10);
    }

    public int handmaiden() {
        int has = (one == Card.HANDMAID || two == Card.HANDMAID) ? 1: 0;
        return has * (4 - current.numPlayers());
    }

    public int baron() {
        int has = (one == Card.BARON || two == Card.BARON) ? 1: 0;
        int high = two.value();

        if (high > 3) {
            return 40;
        }
        return 1;
    }

    public int guard() {
        if (!(one == Card.GUARD || two == Card.GUARD)) {
            return -1;
        }

        Card[] remaining = current.unseenCards();
        double[] proportions = new double[9];

        for (Card c: remaining) {
            proportions[c.value()] = proportions[c.value()] + 1;
        }

        int len = remaining.length;
        int maxi = 2;
        double max = -1;

        for (int i = 2; i < proportions.length; i++) {
            proportions[i] = proportions[i] / len; 
            if (proportions[i] > max) {
                maxi = i;
                max = proportions[i];
            }
            // System.out.println(i + " " + proportions[i]);
        }
        targetCard = maxi;
        return (int) (max * 100);
    }

	public void pickAction() { 

        int[] chromosomes = new int[4];
        
        chromosomes[0] = diff();
        chromosomes[1] = handmaiden();
        chromosomes[2] = guard();
        chromosomes[3] = baron();
        
        // Multiply by the scaling factors
        // Find best option
        int maxi = 0;
        int max  = 0;

        for (int i = 0; i < chromosomes.length; i++) {
            chromosomes[i] *= chrome[i];
            if (chromosomes[i] > max) {
                maxi = i;
                max = chromosomes[i];
            }
        }

        // Chose the best option and play.
        switch (maxi) {
        case 0:
            playLowest();
            break;
        case 1:
            play = Card.HANDMAID;
            break;
        case 2:
            play = Card.GUARD;
            if (myIndex == (target = pickTarget())) {
                play = Card.PRINCE;
            }
            break;
        case 3:
            play = Card.BARON;
            if (myIndex == (target = pickTarget())) {
                play = Card.PRINCE;
            }
            break;
        }
    }       
        

    public void playLowest() {
        // Start with the lowest value card.        
        // Do we have a countess?
        if (myCards.contains(Card.COUNTESS)) { 
            // Is the lowest card > 4
            if (myCards.peek().value() > 4) {
                myCards.remove(Card.COUNTESS);
                play = Card.COUNTESS;
                return;
            }
        }

        // Check if the lowest card needs to be played against someone.
        // Chose a target if required.
        PriorityQueue<Card> copy = new PriorityQueue<Card>(myCards);
        Card low = copy.peek();
//        copy.poll(); // TODO: REMOVE
        Card tmp = Card.PRINCE; // Needs to be initialised could probably be changed
        this.target = myIndex;
        
        while (this.target == myIndex && copy.size() > 0) {
            tmp = copy.poll();
            if (decide(tmp)) {
                this.target = pickTarget();
            }
        }
        if (this.target != myIndex) {
            play = tmp;
            return;
        }
        // No targeted moves -> Play against self.
        // Set play to be prince (since we can only target this against ourself)
        play = Card.PRINCE;
	}
	
	
    public boolean decide(Card myCard){
		switch (myCard) {
			case GUARD:
				return true;
			case PRIEST:
				return true;
			case BARON:
				return true;
			case HANDMAID:
				return false;
			case PRINCE:
				return true;
			case KING:
				return true;
			case COUNTESS:
				return false;
			case PRINCESS:
				return false;
		}
		return false;
	}


	public int pickTarget() {
		// pick player to play card against
//		int testTarg = 0;
//		int noTargets = current.numPlayers();
//		try {
//            // Apply a random factor.
//            int counter = Math.abs((int) System.nanoTime() % noTargets);
//			do {
//				if (counter > 12) {
//					return myIndex;
//				}
//				testTarg = counter % 4;
//				counter++;
//			} while (current.eliminated(testTarg) || current.handmaid(testTarg) || testTarg == myIndex); // while target not eliminated,doesn't have handmaiden, and isn't yourself
//			return testTarg;
        int[] score = new int[current.numPlayers()];
        for (int i = 0; i < current.numPlayers(); i++) {
            score[i] = current.score(i);
        }
        try {
            int max;
            int maxi;
            do {
                max = -1;
                maxi = -1;
                for (int i = 0; i < current.numPlayers(); i++) {
                    if (score[i] >= max && i != myIndex) {
                        maxi = i;
                        max = score[i];
                    }
                }
                score[maxi] = -1; // Reset this so its not used on the next itiration

                // No valid targets.
                if (max == -1) {
                    return myIndex;
                }
            } while (current.eliminated(maxi) || current.handmaid(maxi));
            return maxi;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Target failed");
			return myIndex;
		}
	}
}
