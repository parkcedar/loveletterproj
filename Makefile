# A simple makefile to build this project.
# Default build & run.
# Target: Build
# 	Just build the project
# 		  Run
#   Just run the project
#   	  Docs
#   Complie documentation

SRC = LoveLetter/src
BIN = LoveLetter/bin
DOC = LoveLetter/doc

default: build run

run: 
	@echo 'Running test: '
	java -cp ${BIN} loveletter.GeneticTrainer

build: src/*/*.java
	@echo 'Compiling files '
	javac -d ${BIN} ${SRC}/*/*.java

docs: src/*/*.java
	@echo 'Generate docs'
	javadoc -overview ${DOC}/overview.html -d ${DOC} ${SRC}/*/*.java

loop:
	for i in 1 2 3 4 5 6 7 8 9 10; \
	do					 \
		make run;		 \
		sleep 1; 		 \
	done				

